package uk.co.marksimkins.grapeshot.livecontext;

import uk.co.marksimkins.grapeshot.livecontext.clients.FakeLocalClient;
import uk.co.marksimkins.grapeshot.livecontext.clients.GrapeshotClient;
import uk.co.marksimkins.grapeshot.livecontext.clients.LiveContextClient;

/**
 * Use this factory class to create the {@link LiveContextClient}, which is used to make requests to Grapeshot.
 */
public class LiveContextClientFactory {

    /**
     * Create a new LiveContextClient with the provided API key.
     * @param apiKey The API key to make requests with.
     * @return An initialised {@link LiveContextClient} instance.
     */
    public static LiveContextClient create(String apiKey) {
        // TODO - this properly..!
        //return new FakeLocalClient();
        return new GrapeshotClient(apiKey);
    }

}
