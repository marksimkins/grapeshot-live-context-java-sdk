package uk.co.marksimkins.grapeshot.livecontext.helpers;

import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.RequestBody;
import uk.co.marksimkins.grapeshot.livecontext.models.LinkRelation;

/**
 * Some more generic Http-related utility methods that could be used elsewhere.
 */
public class HttpUtils {

    /**
     * Method to standardise the creation of the UrlBuilder across multiple methods.
     *
     * @param apiPath   The path to the API.
     * @param queryUrl  The URL of the web page being analysed. May be null.
     * @param relation  Zero or more {@link LinkRelation}s to include in the embedded response.
     * @return A suitably-created {@link HttpUrl.Builder}.
     */
    public static HttpUrl.Builder getUrlBuilder(String apiPath, String queryUrl, LinkRelation... relation) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(apiPath).newBuilder();
        for (LinkRelation rel : relation) {
            urlBuilder.addQueryParameter("embed", rel.getParam());
        }
        if (queryUrl != null) {
            urlBuilder.addQueryParameter("url", queryUrl);
        }
        return urlBuilder;
    }

    /**
     * Generate the request containing everything we need to send to Grapeshot.
     *
     * @param urlBuilder    The urlBuilder containing the full API path, plus any required parameters.
     * @param apiKey        The apiKey to make requests against.
     * @param requestBody   The POST-data request body, if required. May be null.
     * @return A properly generated and authorised {@link Request} object.
     */
    public static Request generateRequest(HttpUrl.Builder urlBuilder, String apiKey, RequestBody requestBody) {
        String requestUrl = urlBuilder.build().toString();

        Request.Builder request = new Request.Builder()
                .url(requestUrl)
                .header("Authorization", "Bearer " + apiKey)
                .header("User-Agent", HttpUtils.generateUserAgent());

        if (requestBody != null) {
            request.post(requestBody);
        }

        return request.build();
    }

    private static String generateUserAgent() {
        return String.format("%s/%s", "grapeshot-live-context-java", Version.getVersion());
    }

}
