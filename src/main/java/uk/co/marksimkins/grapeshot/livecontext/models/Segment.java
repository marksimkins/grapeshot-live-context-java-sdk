package uk.co.marksimkins.grapeshot.livecontext.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Defines a segment returned when analysing a URL or text.
 */
public class Segment {

    @JsonProperty
    private String name;
    @JsonProperty
    private double score;
    @JsonProperty("matchterms")
    private List<NameScore> matchTerms;

    /**
     * @return The name of the segment.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The score/weight of the segment.
     */
    public double getScore() {
        return score;
    }

    /**
     * @return The {@link List} of the terms (in {@link NameScore} objects) that matched in the content describing this segment.
     */
    public List<NameScore> getMatchTerms() {
        return matchTerms;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "name='" + name + '\'' +
                ", score=" + score +
                ", matchTerms=" + matchTerms +
                '}';
    }
}
