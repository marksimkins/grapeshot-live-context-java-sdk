package uk.co.marksimkins.grapeshot.livecontext.models;

/**
 * Enum defining the different types of HAL CURIE link relations that may be requested and returned.
 */
public enum LinkRelation {
    SEGMENTS("grapeshot:segments"),
    KEYWORDS("grapeshot:keywords");

    private final String param;

    LinkRelation(String param) {
        this.param = param;
    }

    public String getParam() {
        return param;
    }
}
