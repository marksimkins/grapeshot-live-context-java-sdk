package uk.co.marksimkins.grapeshot.livecontext.clients;

import de.otto.edison.hal.HalParser;
import okhttp3.*;
import uk.co.marksimkins.grapeshot.livecontext.helpers.HttpUtils;
import uk.co.marksimkins.grapeshot.livecontext.models.LinkRelation;
import uk.co.marksimkins.grapeshot.livecontext.exceptions.ConnectionException;
import uk.co.marksimkins.grapeshot.livecontext.models.BaseContextModel;
import uk.co.marksimkins.grapeshot.livecontext.models.KeywordsModel;
import uk.co.marksimkins.grapeshot.livecontext.models.SegmentsModel;

import java.io.IOException;
import java.util.Optional;

import static de.otto.edison.hal.EmbeddedTypeInfo.withEmbedded;

/**
 * An implementation of {@link LiveContextClient} that integrates with the live Grapeshot services.
 */
public class GrapeshotClient implements LiveContextClient {

    private static final String GRAPESHOT_BASE_URL = "https://context-api.grapeshot.com/v1/";
    private static final String GRAPESHOT_PAGE_URL = GRAPESHOT_BASE_URL + "pages";
    private static final String GRAPESHOT_TEXT_URL = GRAPESHOT_BASE_URL + "text";
    private static final String GRAPESHOT_SEGMENTS_URL = GRAPESHOT_BASE_URL + "segments";
    private static final String GRAPESHOT_KEYWORDS_URL = GRAPESHOT_BASE_URL + "keywords";

    private static OkHttpClient client = new OkHttpClient();

    private String apiKey;

    /**
     * Create a new {@link GrapeshotClient}.
     * @param apiKey  The API key which you'll be using to make the requests.
     */
    public GrapeshotClient(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Optional<BaseContextModel> analyseUrl(String url, LinkRelation... relation) throws ConnectionException {
        HttpUrl.Builder urlBuilder = HttpUtils.getUrlBuilder(GRAPESHOT_PAGE_URL, url, relation);
        return sendRequest(urlBuilder, null);
    }

    @Override
    public Optional<BaseContextModel> analyseText(String text, LinkRelation... relation) throws ConnectionException {
        HttpUrl.Builder urlBuilder = HttpUtils.getUrlBuilder(GRAPESHOT_TEXT_URL, null, relation);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("text", text)
                .build();
        return sendRequest(urlBuilder, requestBody);
    }

    @Override
    public Optional<SegmentsModel> getSegments(String url) throws ConnectionException {
        try {
            HttpUrl.Builder urlBuilder = HttpUtils.getUrlBuilder(GRAPESHOT_SEGMENTS_URL, url);
            Request request = HttpUtils.generateRequest(urlBuilder, apiKey, null);
            Response response = client.newCall(request).execute();
            return Optional.of(HalParser.parse(response.body().string()).as(SegmentsModel.class));
        } catch (IOException e) {
            throw new ConnectionException("Exception when attempting Grapeshot connection", e);
        }
    }

    @Override
    public Optional<KeywordsModel> getKeywords(String url) throws ConnectionException {
        try {
            HttpUrl.Builder urlBuilder = HttpUtils.getUrlBuilder(GRAPESHOT_KEYWORDS_URL, url);
            Request request = HttpUtils.generateRequest(urlBuilder, apiKey, null);
            Response response = client.newCall(request).execute();
            return Optional.of(HalParser.parse(response.body().string()).as(KeywordsModel.class));
        } catch (IOException e) {
            throw new ConnectionException("Exception when attempting Grapeshot connection", e);
        }
    }

    // There has to be a way to provide an optional "as" Class, so the two public methods above can be reduced...

    /**
     * Private method to send a general {@link BaseContextModel} request to Grapeshot.
     *
     * @param urlBuilder    The full API url, plus any parameters required.
     * @param requestBody   Any POST-data request body required. May be null.
     * @return              An Optional containing a {@link BaseContextModel} response object, containing the response data.
     * @throws ConnectionException if there was an issue sending or receiving data from Grapeshot.
     */
    private Optional<BaseContextModel> sendRequest(HttpUrl.Builder urlBuilder, RequestBody requestBody) throws ConnectionException {
        try {
            Request request = HttpUtils.generateRequest(urlBuilder, apiKey, requestBody);
            Response response = client.newCall(request).execute();
            return Optional.of(HalParser.parse(response.body().string()).as(BaseContextModel.class,
                    withEmbedded(LinkRelation.KEYWORDS.getParam(), KeywordsModel.class),
                    withEmbedded(LinkRelation.SEGMENTS.getParam(), SegmentsModel.class)));
        } catch (IOException e) {
            throw new ConnectionException("Exception when attempting Grapeshot connection", e);
        }
    }
}
