package uk.co.marksimkins.grapeshot.livecontext.clients;

import de.otto.edison.hal.HalParser;
import uk.co.marksimkins.grapeshot.livecontext.models.LinkRelation;
import uk.co.marksimkins.grapeshot.livecontext.models.BaseContextModel;
import uk.co.marksimkins.grapeshot.livecontext.models.KeywordsModel;
import uk.co.marksimkins.grapeshot.livecontext.models.SegmentsModel;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static de.otto.edison.hal.EmbeddedTypeInfo.withEmbedded;

/**
 * A fake local implementation of {@link LiveContextClient} to emulate suitable functionality.
 */
public class FakeLocalClient implements LiveContextClient {

    private static final String FAKE_URL_RESPONSE = "{\n" +
            "  \"_links\": {\n" +
            "    \"self\": {\n" +
            "      \"href\": \"/v1/pages?url=FAKEURL_ENCODED\"\n" +
            "    },\n" +
            "    \"curies\": [\n" +
            "      {\n" +
            "        \"name\": \"grapeshot\",\n" +
            "        \"href\": \"/rels/grapeshot/{rel}\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"grapeshot:keywords\": {\n" +
            "      \"href\": \"/v1/keywords?url=FAKEURL_ENCODED\"\n" +
            "    },\n" +
            "    \"grapeshot:segments\": {\n" +
            "      \"href\": \"/v1/segments?url=FAKEURL_ENCODED\"\n" +
            "    }\n" +
            "  },\n" +
            "  \"url\": \"FAKEURL\",\n" +
            "  \"status\": \"ok\",\n" +
            "  \"signal_version\": \"4.1.5\",\n" +
            "  \"language\": \"en\"\n" +
            "}";

    private static final String FAKE_TEXT_RESPONSE = "{\n" +
            "  \"_links\": {\n" +
            "    \"self\": {\n" +
            "      \"href\": \"/v1/text?text=\"\n" +
            "    },\n" +
            "    \"curies\": [\n" +
            "      {\n" +
            "        \"name\": \"grapeshot\",\n" +
            "        \"href\": \"/rels/grapeshot/{rel}\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"grapeshot:keywords\": {\n" +
            "      \"href\": \"/v1/keywords?url=\"\n" +
            "    },\n" +
            "    \"grapeshot:segments\": {\n" +
            "      \"href\": \"/v1/segments?url=\"\n" +
            "    }\n" +
            "  },\n" +
            "  \"text\": \"Wikipedia was launched on January 15, 2001, by Jimmy Wales and Larry Sanger. Sanger coined its name, a portmanteau of wiki and encyclopedia. There was only the English-language version initially, but similar versions in other languages quickly developed, which differ in content and in editing practices. With 5,598,054 articles, the English Wikipedia is the largest of the more than 290 Wikipedia encyclopedias. Overall, Wikipedia comprises more than 40 million articles in 299 different languages and, as of February 2014, it had 18 billion page views and nearly 500 million unique visitors each month.\",\n" +
            "  \"status\": \"ok\",\n" +
            "  \"signal_version\": \"4.1.5\",\n" +
            "  \"language\": \"en\",\n" +
            "  \"_embedded\": {\n" +
            "    \"grapeshot:keywords\": {\n" +
            "      \"_links\": {\n" +
            "        \"self\": {\n" +
            "          \"href\": \"/v1/keywords?url=\"\n" +
            "        },\n" +
            "        \"grapeshot:page\": {\n" +
            "          \"href\": \"/v1/pages?url=\"\n" +
            "        },\n" +
            "        \"grapeshot:segments\": {\n" +
            "          \"href\": \"/v1/segments?url=\"\n" +
            "        }\n" +
            "      },\n" +
            "      \"text\": \"Wikipedia was launched on January 15, 2001, by Jimmy Wales and Larry Sanger. Sanger coined its name, a portmanteau of wiki and encyclopedia. There was only the English-language version initially, but similar versions in other languages quickly developed, which differ in content and in editing practices. With 5,598,054 articles, the English Wikipedia is the largest of the more than 290 Wikipedia encyclopedias. Overall, Wikipedia comprises more than 40 million articles in 299 different languages and, as of February 2014, it had 18 billion page views and nearly 500 million unique visitors each month.\",\n" +
            "      \"status\": \"ok\",\n" +
            "      \"signal_version\": \"4.1.5\",\n" +
            "      \"language\": \"en\",\n" +
            "      \"keywords\": [\n" +
            "        {\n" +
            "          \"name\": \"English language\",\n" +
            "          \"score\": 2.133\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Jimmy Wales\",\n" +
            "          \"score\": 2.133\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"page views\",\n" +
            "          \"score\": 2.133\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    \"grapeshot:segments\": {\n" +
            "      \"_links\": {\n" +
            "        \"self\": {\n" +
            "          \"href\": \"/v1/segments?url=\"\n" +
            "        },\n" +
            "        \"grapeshot:page\": {\n" +
            "          \"href\": \"/v1/pages?url=\"\n" +
            "        },\n" +
            "        \"grapeshot:keywords\": {\n" +
            "          \"href\": \"/v1/keywords?url=\"\n" +
            "        }\n" +
            "      },\n" +
            "      \"text\": \"Wikipedia was launched on January 15, 2001, by Jimmy Wales and Larry Sanger. Sanger coined its name, a portmanteau of wiki and encyclopedia. There was only the English-language version initially, but similar versions in other languages quickly developed, which differ in content and in editing practices. With 5,598,054 articles, the English Wikipedia is the largest of the more than 290 Wikipedia encyclopedias. Overall, Wikipedia comprises more than 40 million articles in 299 different languages and, as of February 2014, it had 18 billion page views and nearly 500 million unique visitors each month.\",\n" +
            "      \"status\": \"ok\",\n" +
            "      \"signal_version\": \"4.1.5\",\n" +
            "      \"language\": \"en\",\n" +
            "      \"segments\": [\n" +
            "        {\n" +
            "          \"name\": \"gs_business_marketing\",\n" +
            "          \"score\": 16.226,\n" +
            "          \"matchterms\": [\n" +
            "            {\n" +
            "              \"name\": \"page views\",\n" +
            "              \"score\": 2.133\n" +
            "            }\n" +
            "          ]\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"gs_business\",\n" +
            "          \"score\": 14.276,\n" +
            "          \"matchterms\": [\n" +
            "            {\n" +
            "              \"name\": \"page views\",\n" +
            "              \"score\": 2.133\n" +
            "            }\n" +
            "          ]\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "}";

    private static final String FAKE_SEGMENTS_RESPONSE = "{\n" +
            "  \"_links\": {\n" +
            "    \"self\": {\n" +
            "      \"href\": \"/v1/segments?url=http%3A%2F%2Fwww.grapeshot.com\"\n" +
            "    },\n" +
            "    \"curies\": [\n" +
            "      {\n" +
            "        \"name\": \"grapeshot\",\n" +
            "        \"href\": \"/rels/grapeshot/{rel}\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"grapeshot:page\": {\n" +
            "      \"href\": \"/v1/pages?url=http%3A%2F%2Fwww.grapeshot.com\"\n" +
            "    },\n" +
            "    \"grapeshot:keywords\": {\n" +
            "      \"href\": \"/v1/keywords?url=http%3A%2F%2Fwww.grapeshot.com\"\n" +
            "    }\n" +
            "  },\n" +
            "  \"url\": \"http://www.grapeshot.com\",\n" +
            "  \"status\": \"ok\",\n" +
            "  \"signal_version\": \"4.1.5\",\n" +
            "  \"language\": \"en\",\n" +
            "  \"segments\": [\n" +
            "    {\n" +
            "      \"name\": \"gs_interest_frequent_travelers\",\n" +
            "      \"score\": 53.454,\n" +
            "      \"matchterms\": [\n" +
            "        {\n" +
            "          \"name\": \"MARKETING\",\n" +
            "          \"score\": 1.385\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"industry\",\n" +
            "          \"score\": 1.091\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"YouTube\",\n" +
            "          \"score\": 0.667\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"gs_business\",\n" +
            "      \"score\": 35.2,\n" +
            "      \"matchterms\": [\n" +
            "        {\n" +
            "          \"name\": \"MARKETING\",\n" +
            "          \"score\": 1.385\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"industry\",\n" +
            "          \"score\": 1.091\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"MediaMath\",\n" +
            "          \"score\": 0.667\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"gs_tech\",\n" +
            "      \"score\": 28.964,\n" +
            "      \"matchterms\": [\n" +
            "        {\n" +
            "          \"name\": \"Facebook\",\n" +
            "          \"score\": 0.667\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"Linkedin\",\n" +
            "          \"score\": 0.667\n" +
            "        },\n" +
            "        {\n" +
            "          \"name\": \"YouTube\",\n" +
            "          \"score\": 0.667\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    private static final String FAKE_KEYWORDS_RESPONSE = "{\n" +
            "  \"_links\": {\n" +
            "    \"self\": {\n" +
            "      \"href\": \"/v1/keywords?url=http%3A%2F%2Fwww.grapeshot.com\"\n" +
            "    },\n" +
            "    \"curies\": [\n" +
            "      {\n" +
            "        \"name\": \"grapeshot\",\n" +
            "        \"href\": \"/rels/grapeshot/{rel}\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"grapeshot:page\": {\n" +
            "      \"href\": \"/v1/pages?url=http%3A%2F%2Fwww.grapeshot.com\"\n" +
            "    },\n" +
            "    \"grapeshot:segments\": {\n" +
            "      \"href\": \"/v1/segments?url=http%3A%2F%2Fwww.grapeshot.com\"\n" +
            "    }\n" +
            "  },\n" +
            "  \"url\": \"http://www.grapeshot.com\",\n" +
            "  \"status\": \"ok\",\n" +
            "  \"signal_version\": \"4.1.5\",\n" +
            "  \"language\": \"en\",\n" +
            "  \"keywords\": [\n" +
            "    {\n" +
            "      \"name\": \"Grapeshot\",\n" +
            "      \"score\": 6.185\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"context\",\n" +
            "      \"score\": 3.819\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"insights\",\n" +
            "      \"score\": 3.817\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"next\",\n" +
            "      \"score\": 1.38\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    @Override
    public Optional<BaseContextModel> analyseUrl(String url, LinkRelation... relation) {
        try {
            String replaced = FAKE_URL_RESPONSE
                    .replace("FAKEURL_ENCODED", URLEncoder.encode(url, StandardCharsets.UTF_8.name()))
                    .replace("FAKEURL", url);
            return Optional.of(HalParser.parse(replaced).as(BaseContextModel.class,
                    withEmbedded(LinkRelation.KEYWORDS.getParam(), KeywordsModel.class),
                    withEmbedded(LinkRelation.SEGMENTS.getParam(), SegmentsModel.class)
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<BaseContextModel> analyseText(String text, LinkRelation... relation) {
        try {
            return Optional.of(HalParser.parse(FAKE_TEXT_RESPONSE).as(BaseContextModel.class,
                    withEmbedded(LinkRelation.KEYWORDS.getParam(), KeywordsModel.class),
                    withEmbedded(LinkRelation.SEGMENTS.getParam(), SegmentsModel.class)
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<SegmentsModel> getSegments(String url) {
        try {
            return Optional.of(HalParser.parse(FAKE_SEGMENTS_RESPONSE).as(SegmentsModel.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<KeywordsModel> getKeywords(String url) {
        try {
            return Optional.of(HalParser.parse(FAKE_KEYWORDS_RESPONSE).as(KeywordsModel.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
