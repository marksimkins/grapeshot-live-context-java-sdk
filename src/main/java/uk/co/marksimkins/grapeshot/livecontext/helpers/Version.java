package uk.co.marksimkins.grapeshot.livecontext.helpers;

/**
 * Central place to manage the version of this SDK, used to generate the useragent string sent to Grapeshot.
 */
public class Version {

    /**
     * @return The current version of this SDK.
     */
    public static String getVersion() {
        // This should match the version in the pom.
        return "0.1-SNAPSHOT";
    }

}
