package uk.co.marksimkins.grapeshot.livecontext.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This simply pairs a name and score together into a single object.
 * <p>
 * This is used when returning a list of keywords, and match terms within a segment.
 */
public class NameScore {

    @JsonProperty
    private String name;

    @JsonProperty
    private double score;

    /**
     * @return The name of this item.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The score associated with this item.
     */
    public double getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "NameScore{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
