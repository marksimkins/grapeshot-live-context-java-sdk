package uk.co.marksimkins.grapeshot.livecontext.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.marksimkins.grapeshot.livecontext.clients.LiveContextClient;

import java.util.List;

/**
 * An extension of {@link BaseContextModel} including the a list of Segments returned.
 * <p>
 * This model can be both the main response type (if {@link LiveContextClient#getSegments(String)} is used) and the
 * internal nested type within the "embedded" response of a {@link BaseContextModel}.
 */
public class SegmentsModel extends BaseContextModel {

    @JsonProperty
    private List<Segment> segments;

    /**
     * @return The {@link List} of {@link Segment}s returned in the response.
     */
    public List<Segment> getSegments() {
        return segments;
    }

    @Override
    public String toString() {
        return "SegmentsModel{" +
                "segments=" + segments +
                '}';
    }
}
