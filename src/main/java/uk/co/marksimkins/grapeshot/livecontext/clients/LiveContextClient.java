package uk.co.marksimkins.grapeshot.livecontext.clients;

import uk.co.marksimkins.grapeshot.livecontext.models.LinkRelation;
import uk.co.marksimkins.grapeshot.livecontext.models.BaseContextModel;
import uk.co.marksimkins.grapeshot.livecontext.exceptions.ConnectionException;
import uk.co.marksimkins.grapeshot.livecontext.models.KeywordsModel;
import uk.co.marksimkins.grapeshot.livecontext.models.SegmentsModel;

import java.util.Optional;

/**
 *
 */
public interface LiveContextClient {

    /**
     * Get analyzed information from a web page specified by a URL.
     * <p>
     * This is the main endpoint for analysis requests. Although the returned Page model contains only basic information,
     * you typically will use the embed parameter to specify the desired richer data models to be embedded in the result,
     * for example Segments or Keywords.
     * <p>
     * If the requested URL has been analyzed by our systems, you will get a result with status of "ok" and the analyzed data.
     * <p>
     * If the URL has not yet been analyzed, the status will be "queued" meaning that the data will be available shortly,
     * typically within a minute or two, and can be requested again.
     * <p>
     * If we were unable to reach the specified URL or analyze its content, the status will be "error" and the error_code
     * and error_message fields will explain the error.
     * <p>
     * A status of "over_quota" signifies that you are over the monthly quota for your subscription plan. Note that each
     * embedded model (if any) has its own status and each status should be checked before accessing the data in that model.
     *
     * @param url       The url of the web page to analyse.
     * @param relation  Zero or more {@link LinkRelation}s to include in the embedded response.
     * @return          An Optional containing a {@link BaseContextModel} response object, containing the response data.
     *
     * @throws ConnectionException if there was an issue sending or receiving data from Grapeshot.
     */
    Optional<BaseContextModel> analyseUrl(String url, LinkRelation... relation) throws ConnectionException;

    /**
     * Processes provided text into grapeshot segment and/or keywords. Text over about 1Mb is truncated before processing.
     *
     * @param text      The text to analyse.
     * @param relation  Zero or more {@link LinkRelation}s to include in the embedded response.
     * @return          An Optional containing a {@link BaseContextModel} response object, containing the response data.
     *
     * @throws ConnectionException if there was an issue sending or receiving data from Grapeshot.
     */
    Optional<BaseContextModel> analyseText(String text, LinkRelation... relation) throws ConnectionException;

    /**
     * Get the segments that were analyzed from a web page specified by a URL.
     * <p>
     * Usually you will not need to request this directly. Instead, use {@link #analyseUrl(String, LinkRelation...) analyseUrl}
     * with {@link LinkRelation#SEGMENTS}.
     *
     * @param url       The url of the web page to analyse to return segments.
     *
     * @return          An Optional containing a {@link SegmentsModel} response object, containing the response data.
     * @throws ConnectionException if there was an issue sending or receiving data from Grapeshot.
     */
    Optional<SegmentsModel> getSegments(String url) throws ConnectionException;

    /**
     * Get the keywords that were analyzed from a web page specified by a URL.
     * <p>
     * Usually you will not need to request this directly. Instead, use {@link #analyseUrl(String, LinkRelation...) analyseUrl}
     * with {@link LinkRelation#KEYWORDS}.
     *
     * @param url       The url of the web page to analyse to return segments.
     *
     * @return          An Optional containing a {@link SegmentsModel} response object, containing the response data.
     * @throws ConnectionException if there was an issue sending or receiving data from Grapeshot.
     */
    Optional<KeywordsModel> getKeywords(String url) throws ConnectionException;

}
