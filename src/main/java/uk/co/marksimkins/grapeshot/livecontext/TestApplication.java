package uk.co.marksimkins.grapeshot.livecontext;

import uk.co.marksimkins.grapeshot.livecontext.clients.LiveContextClient;
import uk.co.marksimkins.grapeshot.livecontext.exceptions.ConnectionException;
import uk.co.marksimkins.grapeshot.livecontext.models.*;

import java.util.List;
import java.util.Optional;

/**
 * Just a simple application to test actually *using* the SDK.
 */
public class TestApplication {

    private static final String API_KEY = System.getenv("GRAPESHOT_API_KEY");

    public static void main(String[] args) {
        LiveContextClient client = LiveContextClientFactory.create(API_KEY);
        try {
            String text = "Wikipedia was launched on January 15, 2001, by Jimmy Wales and Larry Sanger. Sanger coined its name, a portmanteau of wiki and encyclopedia. There was only the English-language version initially, but similar versions in other languages quickly developed, which differ in content and in editing practices. With 5,598,054 articles, the English Wikipedia is the largest of the more than 290 Wikipedia encyclopedias. Overall, Wikipedia comprises more than 40 million articles in 299 different languages and, as of February 2014, it had 18 billion page views and nearly 500 million unique visitors each month.";
            String url = "http://www.grapeshot.com";

            System.out.println("=================== ANALYSING TEXT ===================");
            Optional<BaseContextModel> textModel = client.analyseText(text, LinkRelation.SEGMENTS, LinkRelation.KEYWORDS);
            displayCompleteModel(textModel);
            System.out.println();

            System.out.println("=================== ANALYSING URL ===================");
            Optional<BaseContextModel> urlModel = client.analyseUrl(url, LinkRelation.SEGMENTS, LinkRelation.KEYWORDS);
            displayCompleteModel(urlModel);
            System.out.println();

            System.out.println("=================== GETTING KEYWORDS ===================");
            Optional<KeywordsModel> keywordsModel = client.getKeywords(url);
            if (!keywordsModel.isPresent()) {
                System.out.println("No model returned.");
            } else {
                KeywordsModel model = keywordsModel.get();

                System.out.println("Text: " + model.getText());
                System.out.println("URL:  " + model.getUrl());
                System.out.println("Lang: " + model.getLanguage());
                System.out.println("Ver:  " + model.getSignalVersion());
                System.out.println("Stat: " + model.getStatus());
                System.out.println("Keywords: ");

                for (NameScore keyword : model.getKeywords()) {
                    System.out.println("     " + keyword);
                }
            }
            System.out.println();

            System.out.println("=================== GETTING SEGMENTS ===================");
            Optional<SegmentsModel> segmentsModel = client.getSegments(url);
            if (!segmentsModel.isPresent()) {
                System.out.println("No model returned.");
            } else {
                SegmentsModel model = segmentsModel.get();

                System.out.println("Text: " + model.getText());
                System.out.println("URL:  " + model.getUrl());
                System.out.println("Lang: " + model.getLanguage());
                System.out.println("Ver:  " + model.getSignalVersion());
                System.out.println("Stat: " + model.getStatus());
                System.out.println("Segments: ");

                for (Segment segment : model.getSegments()) {
                    System.out.println("     " + segment);
                }
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    private static void displayCompleteModel(Optional<BaseContextModel> modelOpt) {
        if (!modelOpt.isPresent()) {
            System.out.println("No model returned.");
        } else {
            BaseContextModel model = modelOpt.get();

            System.out.println("Text: " + model.getText());
            System.out.println("URL:  " + model.getUrl());
            System.out.println("Lang: " + model.getLanguage());
            System.out.println("Ver:  " + model.getSignalVersion());
            System.out.println("Stat: " + model.getStatus());

            // Would be nice not to have to cast here, if possible.
            List<KeywordsModel> kwrels = model.getEmbedded().getItemsBy(LinkRelation.KEYWORDS.getParam(), KeywordsModel.class);
            for (KeywordsModel rel : kwrels) {
                System.out.println("Found keywords:");
                for (NameScore keyword : rel.getKeywords()) {
                    System.out.println("     " + keyword);
                }
            }

            List<SegmentsModel> snrels = model.getEmbedded().getItemsBy(LinkRelation.SEGMENTS.getParam(), SegmentsModel.class);
            for (SegmentsModel rel : snrels) {
                System.out.println("Found segments:");
                for (Segment segment : rel.getSegments()) {
                    System.out.println("     " + segment);
                }
            }
        }
    }

}
