package uk.co.marksimkins.grapeshot.livecontext.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.otto.edison.hal.HalRepresentation;

/**
 * The "base" model from which all Grapeshot responses derive, that does not include segment or keyword data.
 * <p>
 * These needed to be handled separately, so take a look at {@link SegmentsModel} and {@link KeywordsModel} respectively.
 */
public class BaseContextModel extends HalRepresentation {

    @JsonProperty
    private String url;
    @JsonProperty
    private String text;
    @JsonProperty
    private String status;
    @JsonProperty("signal_version")
    private String signalVersion;
    @JsonProperty
    private String language;

    /**
     * @return The URL being analysed. May be null if this is a text analysis request.
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return The text being analysed. May be null if this is a URL analysis request.
     */
    public String getText() {
        return text;
    }

    /**
     * @return The status message of the API response.
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return The version of the Grapeshot Signal (Live Context) API used by this request.
     */
    public String getSignalVersion() {
        return signalVersion;
    }

    /**
     * @return The ISO 639-1 language code determined from the analysed page.
     */
    public String getLanguage() {
        return language;
    }

    @Override
    public String toString() {
        return "BaseContextModel{" +
                "url='" + url + '\'' +
                ", text='" + text + '\'' +
                ", status='" + status + '\'' +
                ", signalVersion='" + signalVersion + '\'' +
                ", language='" + language + '\'' +
                ", _links=" + getLinks() +
                ", _embedded=" + getEmbedded() +
                '}';
    }
}
