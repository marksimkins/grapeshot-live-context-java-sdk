package uk.co.marksimkins.grapeshot.livecontext.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.marksimkins.grapeshot.livecontext.clients.LiveContextClient;
import java.util.List;

/**
 * An extension of {@link BaseContextModel} including the a list of Keywords returned.
 * <p>
 * This model can be both the main response type (if {@link LiveContextClient#getKeywords(String)} is used) and the
 * internal nested type within the "embedded" response of a {@link BaseContextModel}.
 */
public class KeywordsModel extends BaseContextModel {

    @JsonProperty
    private List<NameScore> keywords;

    /**
     * @return The {@link List} of keywords (in {@link NameScore} objects) returned in the request.
     */
    public List<NameScore> getKeywords() {
        return keywords;
    }

    @Override
    public String toString() {
        return "KeywordsModel{" +
                "keywords=" + keywords +
                '}';
    }
}
