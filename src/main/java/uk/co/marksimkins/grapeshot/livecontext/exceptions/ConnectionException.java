package uk.co.marksimkins.grapeshot.livecontext.exceptions;

/**
 * A local {@link Exception} implementation for Grapeshot connection issues.
 */
public class ConnectionException extends Exception {

    public ConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
