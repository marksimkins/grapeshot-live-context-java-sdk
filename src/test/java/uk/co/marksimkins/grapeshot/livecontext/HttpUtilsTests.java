package uk.co.marksimkins.grapeshot.livecontext;

import okhttp3.HttpUrl;
import okhttp3.Request;
import org.junit.jupiter.api.Test;
import uk.co.marksimkins.grapeshot.livecontext.helpers.HttpUtils;
import uk.co.marksimkins.grapeshot.livecontext.helpers.Version;
import uk.co.marksimkins.grapeshot.livecontext.models.LinkRelation;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HttpUtilsTests {

    @Test
    void testUrlBuilder() throws IOException {
        final String path = "http://api.gs.com/path";
        final String queryUrl = "http://www.test.com";
        HttpUrl.Builder urlBuilder = HttpUtils.getUrlBuilder(path, queryUrl);
        String urlStr = urlBuilder.build().toString();
        assertTrue(urlStr.contains(path));
        assertTrue(urlStr.contains(URLEncoder.encode(queryUrl, StandardCharsets.UTF_8.name())));

        HttpUrl.Builder urlBuilder2 = HttpUtils.getUrlBuilder(path, queryUrl, LinkRelation.SEGMENTS, LinkRelation.KEYWORDS);
        String urlStr2 = urlBuilder2.build().toString();
        assertTrue(urlStr2.contains(path));
        assertTrue(urlStr2.contains(URLEncoder.encode(queryUrl, StandardCharsets.UTF_8.name())));
        assertTrue(urlStr2.contains("embed=" + URLEncoder.encode(LinkRelation.SEGMENTS.getParam(), StandardCharsets.UTF_8.name())));
        assertTrue(urlStr2.contains("embed=" + URLEncoder.encode(LinkRelation.KEYWORDS.getParam(), StandardCharsets.UTF_8.name())));
    }

    @Test
    void testRequestGeneration() {
        final String url = "http://www.example.com/";
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        Request request = HttpUtils.generateRequest(urlBuilder, "apiKey", null);
        assertEquals(request.url().toString(), url);
        assertEquals(request.header("Authorization"), "Bearer apiKey");
        assertEquals(request.header("User-Agent"), "grapeshot-live-context-java/" + Version.getVersion());
    }
}
