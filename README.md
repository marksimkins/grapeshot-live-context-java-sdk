# Grapeshot Live Context Java SDK

This is an UNOFFICIAL Java SDK for the Grapeshot Live Context API, found here: http://dev.grapeshot.com.

It provides a simple means to run URLs and blocks of text through Grapeshot's context systems. The requests are blocking,
and almost certainly not threadsafe.

It's by no means ready for use in production systems - consider this fair warning!

(I'll be improving it over time, adding async options, ensuring threadsafety, improving unit test coverage etc.)

### How do I use it? ###

Firstly, are you sure?! It's not yet uploaded to Maven central. For now you'll have install the jar in the target folder 
into your local project, and include it in your project's classpath.

Then, register at http://dev.grapeshot.com, and get yourself an API Key.

Finally, use `LiveContextClient client = LiveContextClientFactory.create(apiKey);` to get yourself a client.

Explore the methods on the client object. You'll most likely want `analyseUrl` and `analyseText`
and provide appropriate `LinkRelation`s for any segments or keywords you want returned. In this case, 
segments and keywords are returned in the "embedded" data in the response - see below.

The response is in [HAL format](https://github.com/mikekelly/hal_specification/blob/master/hal_specification.md) that 
provides links to other API calls. This can be a little tricky to navigate, so here's a simplistic example of retrieving segments:

```java
Optional<BaseContextModel> urlModel = client.analyseUrl(url, LinkRelation.SEGMENTS);
if (urlModel.isPresent()) {
    BaseContextModel model = urlModel.get();
    List<SegmentsModel> segmentsModels = model.getEmbedded().getItemsBy(LinkRelation.SEGMENTS.getParam(), SegmentsModel.class);
    // There's only going to be at most one, really
    for (SegmentsModel segModel : segmentsModels) {
        System.out.println("Found segments:");
        for (Segment segment : segModel.getSegments()) {
            System.out.println("     " + segment);
        }
    }
}
```